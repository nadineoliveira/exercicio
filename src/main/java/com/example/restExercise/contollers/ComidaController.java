package com.example.restExercise.contollers;

import com.example.restExercise.model.Comida;
import com.example.restExercise.service.ComidaServiceI;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;


@RestController
public class ComidaController {

    Logger logger = LoggerFactory.getLogger(ComidaController.class);

    private ComidaServiceI cs;

    public ComidaController(ComidaServiceI cs) {
        this.cs = cs;
    }

    @GetMapping("/comida")
    public List<Comida> allComida() {
        logger.info("Acedendo a lista de produtos");
        try{
            return cs.allComida();
        }
        catch(Exception e){
            logger.error(e.getMessage());
            logger.info("Erro: "+e.getMessage());
        }
        return null;
    }

    @GetMapping("/comida/{id}")
    public Comida getComidaById(@PathVariable Long id){
        logger.info("Obtendo informacao do produto: "+id);
        try {
            return cs.getComidaById(id);
        }
        catch(Exception e){
            logger.error(e.getMessage());
            logger.info("Erro: "+e.getMessage());
            return null;
        }
    }

    @DeleteMapping("/comida/{id}")
    public void deleteComida(@PathVariable Long id){
        cs.deleteComida(id);
    }

    @PostMapping("/comida")
    public void addComida(@RequestBody Comida novaComida){
        cs.addComida(novaComida);
    }

    @GetMapping("/comida/kcal/{id}")
    public int getKcalOfComida(@PathVariable Long id){
        return cs.getKcalOfComida(id);
    }


    @PutMapping("/comida/{id}")
    public Comida updateComida(@RequestBody Comida novaComida, @PathVariable Long id){
        return cs.updateComida(novaComida,id);
    }



}   
