package com.example.restExercise.service;

import com.example.restExercise.exceptions.ComidaNotFoundException;
import com.example.restExercise.model.Comida;
import com.example.restExercise.repository.ComidaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComidaService implements ComidaServiceI{

    private ComidaRepository cr;

    public ComidaService(ComidaRepository cr) {
        this.cr = cr;
    }

    @Override
    public List<Comida> allComida() {
        return cr.findAll();
    }

    @Override
    public Comida getComidaById(Long id) {
        return cr.findById(id).orElseThrow(()->new ComidaNotFoundException("Comida com id "+id+" não encontrada"));

    }

    @Override
    public void deleteComida(Long id) {
        cr.deleteById(id);
    }

    @Override
    public void addComida(Comida novaComida) {
        cr.save(new Comida(novaComida.getCategoria(), novaComida.getPeso(), novaComida.getMarca(), novaComida.getKcal()));
    }

    @Override
    public int getKcalOfComida(Long id) {
        Comida up = cr.findById(id).get();
        return up.getKcal();
    }

    @Override
    public Comida updateComida(Comida novaComida, Long id) {
        Comida up = cr.findById(id).get();
        up.setCategoria(novaComida.getCategoria());
        up.setPeso(novaComida.getPeso());
        up.setMarca(novaComida.getMarca());
        up.setKcal(novaComida.getKcal());

        return cr.save(up);
    }
}
