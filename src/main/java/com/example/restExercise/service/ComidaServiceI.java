package com.example.restExercise.service;

import com.example.restExercise.model.Comida;

import java.util.List;

public interface ComidaServiceI {

    List<Comida> allComida();
    Comida getComidaById(Long id);
    void deleteComida(Long id);
    void addComida(Comida c);
    int getKcalOfComida(Long id);
    Comida updateComida(Comida c, Long id);
}
