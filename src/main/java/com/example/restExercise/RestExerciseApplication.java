package com.example.restExercise;

import com.example.restExercise.model.Comida;
import com.example.restExercise.service.ComidaServiceI;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

//Rest API sobre comida

@SpringBootApplication
public class RestExerciseApplication {


	public static void main(String[] args) {
		SpringApplication.run(RestExerciseApplication.class, args);
	}

	@Bean
	public CommandLineRunner load(ComidaServiceI cr){
		return(args)->{
			cr.addComida(new Comida("hamburguer",0.200,"lidl", 200));
			cr.addComida(new Comida("cereal", 1.00, "chocapic", 200));
			cr.addComida(new Comida ("cereal", 1.00, "estrelitas", 180));
			cr.addComida(new Comida ("massa",0.5, "milaneza", 250));
			cr.addComida(new Comida("massa", 0.5, "billa", 220));
			cr.addComida(new Comida("arroz", 1.00, "cigala", 300));
		};
	}


}
