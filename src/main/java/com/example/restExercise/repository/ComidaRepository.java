package com.example.restExercise.repository;
import com.example.restExercise.model.Comida;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ComidaRepository extends CrudRepository<Comida,Long>{

    List<Comida> findAll();
    Optional<Comida> findById(Long id);
    void deleteById(Long id);

}
