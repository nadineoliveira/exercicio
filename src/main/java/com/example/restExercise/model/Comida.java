package com.example.restExercise.model;

import java.util.Objects;
import javax.persistence.*;

@Entity

public class Comida {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String categoria;
    private Double peso;
    private String marca;
    private int kcal;

    public Comida(){

    }

   public Comida(String categoria, Double peso, String marca, int kcal) {
       this.categoria = categoria;
       this.peso = peso;
       this.marca = marca;
       this.kcal = kcal;
   }

    public String getCategoria() {
        return categoria;
    }

    public Long getId() {
        return id;
    }

    public Double getPeso() {
        return peso;
    }

    public String getMarca() {
        return marca;
    }

    public int getKcal() {
        return kcal;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setKcal(int kcal) {
        this.kcal = kcal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comida comida = (Comida) o;
        return id == comida.id &&
                kcal == comida.kcal &&
                Objects.equals(categoria, comida.categoria) &&
                Objects.equals(peso, comida.peso) &&
                Objects.equals(marca, comida.marca);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categoria, peso, marca, kcal);
    }
}
