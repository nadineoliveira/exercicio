package com.example.restExercise.exceptions;

public class ComidaNotFoundException extends RuntimeException {

    public ComidaNotFoundException(String message) {
        super(message);
    }
}
